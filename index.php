<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Grain Comics</title>

        <!-- Bootstrap -->
        <link href="libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            header {
                background-color: #000;
                width: 100%;
                height: 120px;
            }
            header #logo {
                margin: 0 auto;
                height: 120px;
                width: auto;
                display: block;
            }
            .centered {
                float: none;
                margin: 0 auto;
            }
            .photo img {
                width: 80%;
                display: block;
                margin: 2em auto;
            }
            .photo .caption {
                text-align: center;
            }
            body {
                background-color: #2a2a2a;
            }
            .item img {
                margin: 1em auto;
            }
        </style>
    </head>
    <body>
        <header>
            <img id="logo" src="imgs/logo.jpeg" />
        </header>



        <div id="comic-1" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#comic-1" data-slide-to="0" class="active"></li>
                <li data-target="#comic-1" data-slide-to="1"></li>
                <li data-target="#comic-1" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="photos/01.jpg" alt="...">
                    <div class="carousel-caption">
                        <em>This is caption for photo #1</em>
                    </div>
                </div>
                <div class="item">
                    <img src="photos/02.jpg" alt="...">
                    <div class="carousel-caption">
                        <em>This is caption for photo #2</em>
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#comic-1" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#comic-1" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="libs/bootstrap/dist/js/bootstrap.min.js"></script>
        <!--
        <script>
            $(document).on('ready', function() {
                $('#comic-1').carousel({
                    interval: false
                });
            });
        </script>
        -->
    </body>
</html>